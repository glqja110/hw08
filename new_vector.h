#ifndef _NEW_VECTOR_H
#define _NEW_VECTOR_H

#include <iostream> 
#include <vector>
#include <sstream>
#include <typeinfo>

using namespace std; 

class NewVector 
{ 
public: 
    NewVector(){count_ = 0;} 
    template <typename T> 
    void add(T _value) { // implement ++count_; 
	if((typeid(_value).name())[0]=='i') {     //int형 입력했을 때
		int value=_value;
		stringstream ss;
		ss<<value;
		string index=ss.str();
		list.push_back(index);
		count_++;
	}
	else if((typeid(_value).name())[0]=='d') {    //double형 입력했을 때
		double value=_value;
		ostringstream strs;
		strs<<value;
		string index=strs.str();
		list.push_back(index);
		count_++;
	}
	else if((typeid(_value).name())[0]=='c') {    //char형 입력했을 때
		string index;
		index+=_value;
		list.push_back(index);
		count_++;
	}

    }
    inline int count() const {return count_;}

    string contents(int idx) const {return list[idx];}

    friend ostream& operator << (ostream& _os, const NewVector _nv); 
   
private: 
    int count_; 
    vector<string> list;
    // add your own private function / variables 
};

#endif /* new_vector.h */
