#ifndef _SORTED_ARRAY_H
#define _SORTED_ARRAY_H

#include <iostream> 
#include <string> 
#include <map> 
#include <typeinfo>
#include <vector>

using namespace std; 

vector<string> inputs; //입력 한 내용들 들어가는 벡터

template <typename T> 
class SortedArray { 
  public: 
  	SortedArray() {
	size_=0;
	alloc_=0;
	}
 
  	SortedArray(const SortedArray& a) {
	size_=a.size();
	alloc_=0;
	}
  	SortedArray(size_t size) {
	size_=size;
	alloc_=0;	
	}
  	~SortedArray() {}
  	SortedArray& operator=(const SortedArray& a) {     //int_arrays[str] = array 이런곳에서 쓰일 연산자. 오른쪽에 있는 데이터들을 다 왼쪽으로 옮겨 넣어줘야함 여기가 중요하다...
		size_=a.size();     //사이즈 저장.
		values_=new T[size_];
		for(int i=0;i<a.size();i++) {
			values_[i]=a(i);
			
		}	
	}
  	size_t size() const { return size_; }

  	const T& operator()(int idx) const {
		return values_[idx];
	}
 	
  	void Reserve(int size) { //  주어진  크기만큼  미리  메모리  할당.
	size_=size; 
	values_=new T[size_];   //메모리 할당
	}
	
  	void Add(const T& value) {
		values_[alloc_]=value;
		alloc_++;
	}
 	
  	int Find(const T& value) {
		int position=-1;
		if(typeid(values_[0]).name()=="i"||typeid(values_[0]).name()=="d") {
			int left=0,right=size_,mid;
			while(left<=right) {
				mid=(left+right)/2;
		
				if(value<values_[mid]) right=mid-1;
				else if(values_[mid]<value) left=mid+1;
				else {
					position=mid;
				}
			}
		}
		else {
			for(int i=0;i<size_;i++) {
				if(value==values_[i]) {
					position=i;
				}
			}
		}
		return position;
	}
  	//  주어진  값의  위치,  없으면 ­1 을  리턴. 
  	// Binary Search 를  사용해야  함 
	void sort() {   //원소들 정렬하기
		string chec=typeid(values_[0]).name();   
		char t=chec[0];
		if((int)t==105) {   //int형이면
			T tmp;
			for(int i=0;i<size_-1;i++)
			{
				for(int j=0;j<size_-1;j++)
				{
					if(values_[j]>values_[j+1])
					{
						tmp=values_[j];
						values_[j]=values_[j+1];
						values_[j+1]=tmp;
					}
				}
			}
		}
		else if((int)t==100) { //double형이면
			T tmp;
			for(int i=0;i<size_-1;i++)
			{
				for(int j=0;j<size_-1;j++)
				{
					if(values_[j]>values_[j+1])
					{
						tmp=values_[j];
						values_[j]=values_[j+1];
						values_[j+1]=tmp;
					}
				}
			}	
		}
		else if((int)t==83) { //string형이면
			T tmp;
			for(int i=0;i<size_-1;i++)
			{
				for(int j=0;j<size_-1;j++)
				{
					int k=0;
					if(values_[j]>values_[j+1])   { //a랑 b 비교 같은거
						tmp=values_[j];
						values_[j]=values_[j+1];
						values_[j+1]=tmp;
					}
	
				}
			}
		}
	}
	
 
  private: 
  	T* values_; //배열
	vector<string> inputs; //입력 한 내용들 들어가는 벡터
  	int size_, alloc_; //크기들 
	int pos;  //가리키고 있는 위치
}; 

template <typename T> 
istream& operator>>(istream& is, SortedArray<T>& a) {     //입력
	int num;
	is>>num;
	vector<T> inputss(num);
	a.Reserve(num);
	for(int i=0;i<num;i++) is>>inputss[i];            //inputs에 우선 저장해둠.
	for(int i=0;i<num;i++) a.Add(inputss[i]);           //입력한것들 Add함수 이용해서 추가함
	a.sort();  //정렬해준다
	return is;
}
 
template <typename T> 
ostream& operator<<(ostream& os, const SortedArray<T>& a) {
	for(int i=0;i<a.size();i++) {
		os<<a(i)<<' ';
	}
	return os;
}

#endif /* sorted_array.h */

