// new_vector_main.cc 
#include "new_vector.h" 
using namespace std; 
 
int main(void) 
{ 
    NewVector nv = NewVector(); 
    while(true) 
    { 
        string type; 
        cin >> type; 
 
 
        if(type == "i")     //int형
        { 
            int value; 
            cin >> value; 
            nv.add(value); // replace to nv.add<int>(value) when error occurs 
        } 
        else if(type == "d")   //double형
        { 
            double value; 
            cin >> value; 
            nv.add(value); // replace to nv.add<double>(value) when error occurs 
        } 
        else if(type == "c")   //char형
        { 
            char value; 
            cin >> value; 
            nv.add(value); // replace to nv.add<char>(value) when error occurs 
        } 
        else if(type == "print") cout << nv; 
        else if(type == "q") break; 
    } 
    return 0; 
}
